var mysql = require("mysql");
var DB = require('../config/database.js').DB;

connection = mysql.createConnection(DB.info.mysqldb1);

var Cc = {};

Cc.add = function (centrocosto, callback) 
{
    if (connection) {
        connection.query("INSERT INTO info_centros_costos SET ?", centrocosto, function (error, result) 
        {
            console.log(result);
            if (error) {
                throw error;
            }
            else {
                return callback(null, result.insertId);
            }
        })
    }
}

Cc.findcC = function (num,callback) {
    if (connection) {

        var sql = "SELECT CAST(centro_costo AS CHAR) as cc , info_centros_costos.* FROM info_centros_costos WHERE centro_costo LIKE '%" + num + "%' LIMIT 10";
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

Cc.get = function (sql_area , sql_personas , sql_items , callback) 
{

    if (connection) 
    {
        var sql = 'select  us_resp.nombre nombre_resp , us_resp.apellido apellido_resp , areas.descripcion area , CONCAT_WS(" ", us.nombre , us.apellido ) nombre_jefe_proyecto , cc.* from info_centros_costos cc LEFT JOIN info_users us_resp on cc.responsable = us_resp.id_usuario INNER JOIN info_users us on cc.cc_jefe_proyecto = us.id_usuario INNER JOIN info_areas_unidad_de_negocio areas on cc.cc_unidad_negocio = areas.id_area where cc.estado = 1 ' + sql_items + sql_area + sql_personas + ' ORDER BY FIELD(cc_unidad_negocio , 4,2,5,8,7,9,1,6)';
        connection.query(sql, function (error, row) 
        {
            if (error) {
                
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

Cc.getValidar = function (callback) 
{

    if (connection) 
    {

        var sql = 'Select areas.descripcion area , CONCAT_WS(" ", us.nombre , us.apellido ) nombre_jefe_proyecto , cc.* from info_centros_costos cc INNER JOIN info_users us on cc.cc_jefe_proyecto = us.id_usuario INNER JOIN info_areas_unidad_de_negocio areas on cc.cc_unidad_negocio = areas.id_area where cc.estado = 0 ORDER BY cc_jefe_proyecto DESC';
        connection.query(sql, function (error, row) {
            if (error) {

                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

Cc.getAllUser = function ( consulta , callback) {

    if (connection) 
    {
        var sql = 'Select areas.descripcion area , CONCAT_WS(" ", us.nombre , us.apellido ) nombre_jefe_proyecto , cc.* from info_centros_costos cc INNER JOIN info_users us on cc.cc_jefe_proyecto = us.id_usuario INNER JOIN info_areas_unidad_de_negocio areas on cc.cc_unidad_negocio = areas.id_area where cc.estado = 1 ' + consulta + ' ORDER BY cc.cc_jefe_proyecto DESC';
        
        console.log(sql);
        
        connection.query(sql, function (error, row) 
        {
            if (error) {

                throw error;
            }
            else {

                if (row.length > 0) 
                {
                    return callback(null, row);
                }
                    
                return callback(null, [] );

            }
        });
    }
}


Cc.getEmpresas = function ( name , callback) 
{
    if (connection) {
        var sql = "SELECT *  FROM info_empresas user WHERE name LIKE '%" + name + "%' LIMIT 10";
        connection.query(sql, function (error, row) 
        {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}


Cc.getNumCC = function (callback) 
{
    if (connection) {
        var sql = "select  (max(centro_costo) + 1) AS centro_costo from info_centros_costos";
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

 
Cc.getId = function (id,callback) 
{
    if (connection) {
        var sql = "Select emp.name nombre_empresa , CONCAT_WS(' ', users2.nombre , users2.apellido  ) jefe_area  , CONCAT_WS(' ', users.nombre , users.apellido  ) jefe_proyecto  , cc.* from info_centros_costos cc INNER JOIN info_users users on cc.cc_jefe_proyecto = users.id_usuario  INNER JOIN info_users users2 on cc.cc_jefe_area = users2.id_usuario INNER JOIN info_empresas emp on cc.cc_empresa = emp.id_harvest  where id_proyecto =" + id;
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else { 
                return callback(null, row);
            }
        });
    }
}

Cc.getInactivos = function (id , callback) 
{
    if (connection) {

        var sql = "Select * from info_centros_costos Where estado = " + id + " ORDER BY id_proyecto DESC LIMIT 100";
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}


Cc.put = function (name, data, id, callback) {
    if (connection) {
        sql = "UPDATE info_centros_costos SET " + name + "= '" + data + "' where id_proyecto  = " + id;
        connection.query(sql, function (error, result) {
            console.log(result);

            if (error) {
                throw error;
            }
            else {
                return callback(null, "Usuario actualizado");
            }

        })

    }
}


Cc.putActivacin = function (cc, id, callback) {
    if (connection) {
        sql = "UPDATE info_centros_costos SET estado = 1 , fecha_activacion = " + Math.round((Date.now() / 1000 )) +  " , centro_costo = " + cc  + "  where id_proyecto  = " + id;

        connection.query(sql, function (error, result) {
            console.log(result);

            if (error) {
                throw error;
            }
            else {
                return callback(null, "Usuario actualizado");
            }

        })

    }
}


//EMPRESAS

Cc.addEmpresas = function (empresas, callback) {
    if (connection) {
        connection.query("INSERT INTO info_empresas SET ?", empresas, function (error, result) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, result.insertId);
            }
        })
    }
}


//HISTORIAL

Cc.addHistorial = function (centrocosto, callback) {
    if (connection) {
        connection.query("INSERT INTO info_historial_centro_costo SET ?", centrocosto, function (error, result) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, result.insertId);
            }
        })
    }
}


Cc.getHistorial = function (id , callback) 
{
    if (connection) {
        var sql = "Select from_unixtime(hcc.create_at) as fecha , CONCAT_WS(' ', users.nombre , users.apellido ) nombre ,  hcc.*  from info_historial_centro_costo hcc INNER JOIN info_users users on hcc.id_usuario = users.id_usuario  WHERE hcc.id_centro_costo = " + id + " ORDER BY create_at DESC ";
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

Cc.getFaturar = function (id, callback)
{
    if (connection) {

        var sql = "select sum(monto) as monto from info_facturar where estado = 2 and cc = " + id;
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}


Cc.getFaturarSolicitadas = function (key , callback)
{
    if (connection) 
    {
        if(key == 1)
        {
            var sql = "SELECT CONCAT_WS(' ', users.nombre , users.apellido ) nombre  ,  DATE_FORMAT(fact.fecha_solicita , '%d-%m-%Y') f_solicita , DATE_FORMAT(fact.fecha_realizado , '%d-%m-%Y') f_realizado , fact.* FROM info_facturar fact INNER JOIN info_users users on fact.id_solicita = users.id_usuario  where fact.estado = " + key + " ORDER BY fact.fecha_solicita DESC LIMIT 50";
        }

        if(key == 2)
        {
            var sql = "SELECT CONCAT_WS(' ', users.nombre , users.apellido ) nombre   ,  DATE_FORMAT(fact.fecha_solicita , '%d-%m-%Y') f_solicita , DATE_FORMAT(fact.fecha_realizado , '%d-%m-%Y') f_realizado , fact.* FROM info_facturar fact INNER JOIN info_users users on fact.id_realizado = users.id_usuario  where fact.estado = " + key + " ORDER BY fact.fecha_solicita DESC LIMIT 50 ";

        }
        
        console.log('HOLA' + sql);
        
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, row);
            }
        });
    }
}

Cc.addFacturacion = function (data, callback) {
    if (connection) 
    {
        connection.query("INSERT INTO info_facturar SET ?", data, function (error, result)
        {

            if (error) {
                throw error;
            }
            else {
                return callback(null, result.insertId);
            }
        })
    }
}


Cc.putFactura = function (id, monto , oc , detalle , callback) 
{
    if (connection) 
    {

        sql = "UPDATE info_facturar SET oc  = '" + oc + "' , monto = '" + monto + "'  , detalle = '" + detalle +"'   where id  = " + id;

        connection.query(sql, function (error, result) {

            if (error) {
                throw error;
            }
            else {
                return callback(null, 1);
            }

        })

    }
}

Cc.putFacturaEstado = function (id, estado , callback) 
{
    if (connection) {

        sql = "UPDATE info_facturar SET  estado = '" + estado + "'   where id  = " + id;

        connection.query(sql, function (error, result) {

            if (error) {
                throw error;
            }
            else {
                return callback(null, 1);
            }

        })

    }
}

Cc.putFacturaAceptado = function (id, num_factura ,  estado , id_realizado , fecha_realizado , callback) {
    if (connection)
    {

        sql = "UPDATE info_facturar SET num_factura  = " + num_factura + " , estado = '" + estado + "'  , id_realizado = '" + id_realizado + "' ,  fecha_realizado = '" + fecha_realizado + "'  where id  = " + id;
        
        console.log(sql);

        connection.query(sql, function (error, result) {

            if (error) {
                throw error;
            }
            else {
                return callback(null, 1);
            }
        })

    }
}






module.exports = Cc;