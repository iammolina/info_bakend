var Envios = {};
var Sendgrid = require('sendgrid');
var helper = require('sendgrid/lib/helpers/mail/mail');


Envios.mail_validar_centro_costo = function (id , nombre , template , callback)
{
    const mailer = Sendgrid('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');
    // Create the email object
    const email = new helper.Mail();
    // Set the from address
    const fromEmail = new helper.Email('infocl@instore.cl');
    email.setFrom(fromEmail);
    // Set the email subject
    email.setSubject('[prueba]' + 'Validacion');
    // Set the content - even if your template doesn't use content, the API requires at least one character to be sent
    const content = new helper.Content('text/html', 'Template');

    email.addContent(content);
    // Create some personalization for the email
    const personalization = new helper.Personalization();


    const firstName = new helper.Substitution('%id%', id );
    personalization.addSubstitution(firstName);

    const jefeArea = new helper.Substitution('%jefe_area%', nombre);
    personalization.addSubstitution(jefeArea);

    // Add a to address
    const toEmail = new helper.Email('jmolina@instore.cl');
    personalization.addTo(toEmail);

    // Add all of these personalizations to the email
    email.addPersonalization(personalization);
    // Specify which template you want to use
    email.setTemplateId(template);
    // Put everything together into an email request object
    const request = mailer.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: email.toJSON(),
    });
    // Send the email!
    mailer.API(request, (e, response) => 
    {
        return callback(e,response);
    });
};


module.exports = {
    Envios: Envios
};