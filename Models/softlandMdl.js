var mysql = require("mysql");
const sql = require('mssql');
var DB = require('../config/database.js').DB;

connection = mysql.createConnection(DB.info.mysqldb1);
config = 'mssql://sa:Softland2012@10.100.100.11/INSTORESA';
config_iconstruye = 'mssql://sa:Softland2012@10.100.100.11/intore_prueba';



var Soft = {};

Soft.getTotalNotasdeVentas = function (id , callback) 
{
    new sql.ConnectionPool(config).connect().then(pool => {
        return pool.request().query('SELECT sum(nv.nvMonto) as monto , round((sum(nv.nvMonto) / 1.19 ) , 0) as monto_neto FROM softland.nw_nventa nv WHERE SUBSTRING(nv.CodiCC, 4, 10) * 1 = ' + id)
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        callback(null, err);
        sql.close();
    });    
   
}



Soft.getNotasdeVentas = function (id , callback) {

    new sql.ConnectionPool(config).connect().then(pool => 
    {
        return pool.request().query('SELECT  nv.SolicitadoPor as fecha_recep, nv.DespachadoPor as fecha_emision,nv.NVNumero as nvnumero, nv.nvFem as nvfem, nv.NumOC as numoc, aux.NomAux as codaux, nv.CodMon as codmon, SUBSTRING(nv.CodiCC, 4, 10) * 1 as codicc, (nv.nvNetoAfecto * nv.nvEquiv) as neto, nv.nvMonto as monto FROM softland.nw_nventa nv, softland.cwtauxi aux WHERE SUBSTRING(nv.CodiCC, 4, 10) * 1 = ' + id + ' AND nv.CodAux = aux.CodAux ORDER BY nv.nvFem')
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        callback(null, err);
        sql.close();
    });

}




Soft.getTotalOcSoftland = function (id , callback) 
{
    new sql.ConnectionPool(config).connect().then(pool => {
        return pool.request().query("SELECT  sum(owordencom.ValorTotOC) as monto  FROM softland.owordencom owordencom  WHERE RIGHT(owordencom.CodiCC , 5)  = " +  id)
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        callback(null, err);
        sql.close();
    });

}


Soft.getOcSoftland = function (id,callback) {

    new sql.ConnectionPool(config).connect().then(pool => {
        return pool.request().query("SELECT owordencom.FechaOC as FechaOC,  (owordencom.ValorTotOC - (owordencom.NetoAfecto * 1.19)) as totalExcento, cwtauxi.CodAux as CodAux, cwtauxi.NomAux as NomAux, owordencom.NumOC as NumOC, owobsoc.ObservOC as ObservOC, owordencom.NetoAfecto as NetoAfecto, owordencom.NetoExento as NetoExento, owordencom.NomCon as NomCon, owordencom.ValorTotOC as ValorTotOC, owordencom.CodiCC as CodiCC, Cwtccos.DescCC as proyecto  FROM softland.owobsoc owobsoc, softland.cwtauxi cwtauxi, softland.owordencom owordencom, softland.cwtccos cwtccos  WHERE owordencom.NumInterOC = owobsoc.NumInterOC  AND owordencom.CodAux = cwtauxi.CodAux  AND owordencom.CodiCC = cwtccos.CodiCC  AND RIGHT(owordencom.CodiCC , 5) = " + id + " AND owordencom.CodEstado <> 'UN'")
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        callback(null, err);
        sql.close();
    });
}


Soft.getOcIcontruye = function (id,callback) {

    new sql.ConnectionPool(config_iconstruye).connect().then(pool => {
        return pool.request().query("SELECT oc.IDOC , oc.NUMOC , oc.NOMOC , oc.FECHACREACION , oc.total as total , oc.DESCUENTOS as descuentos , ( oc.total - oc.DESCUENTOS ) as total_final FROM oc WHERE(oc.IDESTADODOC In(44, 46, 47, 49, 53)) AND SUBSTRING(oc.NUMOC, 1, 5) = '"+ id +"'")
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        
        console.log(err);

        callback(null, err);

        sql.close();
    });

}

Soft.getTotalOcIconstruye = function (id,callback) 
{
    new sql.ConnectionPool(config_iconstruye).connect().then(pool => {
        return pool.request().query("SELECT   ( sum(oc.total) - Sum(oc.DESCUENTOS) ) as monto FROM oc WHERE(oc.IDESTADODOC In(44, 46, 47, 49, 53)) AND SUBSTRING(oc.NUMOC, 1, 5) = '" + id+"'")
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        console.log(err);
        callback(null, err);
        sql.close();
    });
}


Soft.getSubcontratos = function (id,callback)
{

    new sql.ConnectionPool(config_iconstruye).connect().then(pool => {
        return pool.request().query("SELECT  subcontratos.IDDOC , subcontratos.NUMDOC ,subcontratos.NOMDOC , subcontratos.MONTONETO , subcontratos.MONTODESCUENTO , subcontratos.FECHAINICIO , ( subcontratos.MONTONETO - subcontratos.MONTODESCUENTO ) as total_final FROM subcontratos WHERE(IDESTADODOC in (173, 175, 176, 177)) AND SUBSTRING(subcontratos.NUMDOC, 1, 5) ='" + id + "'")
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {

        callback(err, null);
        sql.close();
    });

}

Soft.getTotalSubcontratos = function (id,callback) {
    new sql.ConnectionPool(config_iconstruye).connect().then(pool => {
        return pool.request().query("SELECT  (SUM(subcontratos.MONTONETO) - sum(subcontratos.MONTODESCUENTO)) AS monto FROM subcontratos WHERE(IDESTADODOC in (173, 175, 176, 177)) AND SUBSTRING(subcontratos.NUMDOC, 1, 5) ='" + id+"'")
    }).then(result => {
        let rows = result.recordset
        callback(null, rows);
        sql.close();
    }).catch(err => {
        console.log(err);
        callback(err, null);
        sql.close();
    });
}


Soft.addCentrocosto = function ( cc , nombre , callback) 
{
    new sql.ConnectionPool(config).connect().then(pool => {
        const sql = "INSERT INTO softland.cwtccos(CodiCC, DescCC, NivelCC, Activo) VALUES( '" + cc + "' , '" + nombre + "' , 2, 'S') ";
        return pool.request().query(sql);
    }).then(result => {
        
        let rows = result.rowsAffected
        callback(null, rows);
        sql.close();

    }).catch(err => 
        {
        callback(err, null);
        sql.close();
    });
}



module.exports = Soft;