var mysql = require("mysql");
var DB = require('../config/database.js').DB;

connection = mysql.createConnection(DB.info.mysqldb1);

var User = {};

User.findOneById = function (username, password, callback) {
    if (connection) {
        var sql = "SELECT * FROM info_users INNER JOIN info_permisos_usuarios on info_users.id_usuario = info_permisos_usuarios.id_usuarios WHERE info_users.correo = " + connection.escape(username) +
            "AND info_users.clave = " + connection.escape(password);

        connection.query(sql, function (error, row) 
        {
            if (error) {
                throw error;
            }
            else {

                return callback(null, row);
            }
        });

    }
} 

User.find = function (user , callback) {
    if (connection) {
        var sql = "SELECT CONCAT_WS(' ', user.nombre ,  user.apellido ) as nombre_usuario , user.*  FROM info_users user WHERE nombre LIKE '%" + user + "%' LIMIT 10";
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {

                return callback(null, row);
            }
        });

    }
} 

User.findIdOld = function (id, callback) {
    if (connection) {
        var sql = "SELECT * FROM info_users WHERE id_anterior = " + id;
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {


                return callback(null, row);
            }
        });

    }
} 

User.findMail = function (id, callback) {
    if (connection) {
       
        var sql = "select infous.correo email_j_area , infous2.correo email_j_proyecto from info_centros_costos infocc INNER JOIN info_users infous on infocc.cc_jefe_area = infous.id_usuario INNER JOIN info_users infous2 on infocc.cc_jefe_proyecto = infous2.id_usuario where infocc.id_proyecto =" + id;
       
        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {

                return callback(null, row);
            }
        });

    }
}

User.add = function (userdata, callback) {
    if (connection) 
    {
        connection.query("INSERT INTO info_users SET ?", userdata, function (error, result) {
            if (error) {
                throw error;
            }
            else {
                return callback(null, result.insertId);
            }
        })
    }
}

User.permisos = function (id, callback) {
    if (connection) {


        var sql = "Select * from info_permisos_usuarios where id_usuarios = " + id;

        connection.query(sql, function (error, row) {
            if (error) {
                throw error;
            }
            else {

                return callback(null, row);
            }
        });

    }
} 




module.exports = User;