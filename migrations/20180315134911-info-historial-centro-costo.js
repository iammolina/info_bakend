'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) 
{

  db.createTable('info_historial_centro_costo',
    {
      id_historial: { type: 'int', primaryKey: true, autoIncrement: true },
      id_usuario : { type: 'int'},
      id_centro_costo: { type: 'int' },
      observacion: { type: 'string' },
      create_at: { type: 'int' }
    }, callback);

  
};

exports.down = function(db) {
  return db.dropTable('info_historial_centro_costo');
};

exports._meta = {
  "version": 1
};
