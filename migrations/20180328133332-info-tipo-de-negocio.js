'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db,callback) {
  
  db.createTable('info_tipo_de_negocios',
    {

      id_tipo_negocio: { type: 'int', primaryKey: true, autoIncrement: true },
      descripcion: { type: 'string' },
      create_at: { type: 'int' },
      updated_at: { type: 'int' }

    }, callback);


};

exports.down = function(db) {


  return db.dropTable('info_tipo_de_negocios');
  

};

exports._meta = {
  "version": 1
};
