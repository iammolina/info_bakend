'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db,callback) 
{

  db.createTable('info_users',
    {
      id_usuario: { type: 'int', primaryKey: true, autoIncrement: true },
      id_anterior: { type: 'int' },
      nombre: { type: 'string' },
      apellido: { type: 'string' },
      correo: { type: 'string' },
      clave: { type: 'string' },
      telefono: { type: 'string' },
      rut: { type: 'string' },
      jefe_area: { type: 'int', defaultValue: 0 },
      jefe_proyecto: { type: 'int', defaultValue: 0 },
      create_at: { type: 'int' },
      updated_at:{ type: 'int' }

    }, callback);
  
};

exports.down = function(db) {

  return db.dropTable('info_users');

};

exports._meta = {
  "version": 1
};
