'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db,callback) 
{

  db.createTable('info_permisos_usuarios',
    {

      id_permisos: { type: 'int', primaryKey: true, autoIncrement: true },
      id_usuarios: { type: 'string' },
      ver_todos_proyectos: { type: 'int', defaultValue: 0 },
      ver_propios_proyectos: { type: 'int', defaultValue: 0 },
      ver_areas_proyectos: { type: 'int', defaultValue: 0 },
      create_at:   { type: 'int' },
      updated_at:  { type: 'int' }

    }, callback);

  
};

exports.down = function(db) 
{
  return db.dropTable('info_permisos_usuarios');
};

exports._meta = {
  "version": 1
};
