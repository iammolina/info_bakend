'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db,callback) 
{
  db.createTable('info_centros_costos', 
  {
    id_proyecto: { type: 'int', primaryKey: true, autoIncrement: true },
    centro_costo: { type: 'int' },
    cc_nombre_proyecto: { type: 'string' },
    cc_ubicacion: { type: 'string' },
    cc_cliente: { type: 'string' },
    cc_direccion: { type: 'string' },
    cc_superficie: { type: 'string' },
    cc_unidad_negocio: { type: 'string' },
    cc_tipo_negocio: { type: 'string' },
    cc_jefe_area: { type: 'int' },
    cc_jefe_proyecto: { type: 'int' },
    activo: { type: 'int', defaultValue: 0 },
    estado: { type: 'int', defaultValue: 0},
    fecha_activacion: { type: 'string' },
    cc_fecha_planimetria: { type: 'int' },
    cc_fecha_presupuesto: { type: 'int' },
    cc_fecha_inicio: { type: 'int' },
    cc_fecha_termino: { type: 'int' },
    create_at: { type: 'int' }
  }, callback);
};

exports.down = function(db) {
  return db.dropTable('info_centros_costos');
};

exports._meta = {
  "version": 1
};