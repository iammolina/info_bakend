'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) 
{

   db.createTable('info_facturar',
    {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      cc: { type: 'int' },
      porciento: { type: 'int' },
      fecha_solicita: { type: 'string' },
      fecha_realizado: { type: 'string' },
      id_solicita: { type: 'int' },
      id_realizado: { type: 'int' },
      num_factura: { type: 'int' },
      estado: { type: 'int' },
      detalle: { type: 'string' },
      monto: { type: 'int' },
      oc: { type: 'int' },
    }, callback);

  
};

exports.down = function(db) {

  return db.dropTable('info_facturar');

};

exports._meta = {
  "version": 1
};
