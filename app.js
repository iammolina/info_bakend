var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var Config = require('./config/config').Config;
var index = require('./routes/index');
var users = require('./routes/users');
var centroCosto = require('./routes/centroCosto');
var integraciones = require('./routes/integracion');
var facturar = require('./routes/facturar');


var app = express();
app.use(express.static('public'));

app.use(cors());

app.use(cookieParser());

app.use(require('express-session')({ secret: Config.info.mySecretKey}));

app.use(bodyParser.json({ limit: "50mb" }));

app.use(bodyParser.urlencoded({ extended: false }));

app.use('/users', users);

// app.use(function(req,res,next) 
// {

 
//   if (req.headers.authorization) 
//   {
//     var token = Config.Validate_token(req);

//     jwt.verify(token, Config.info.mySecretKey , function (error, decoded) {
//       if (error) 
//       {
//         res.status(401).json("La key es invalida");
//       }
//       else 
//       {
//         next();
//       }
//     });
//   }
//   else 
//   {

//     res.status(401).json("No tienes acceso , falta key");

//   }
  
// })

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', index);

app.use('/centrocosto',centroCosto);
app.use('/integracion',integraciones);
app.use('/facturar', facturar);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
