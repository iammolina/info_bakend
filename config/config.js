var Config = {};

Config.info =
    {
        mySecretKey: 'ypL1Tq16Ft5846O8FBEc28rx5R6V5Hln'
    };


Config.Validate_token = function (req) 
{
    var token = null;
    var authorization = req.headers.authorization.split(" ");

    if (authorization.length === 2)
    {
        var key = authorization[0];
        var val = authorization[1];

        if (/^Bearer$/i.test(key)) {
            token = val.replace(/"/g, "");
            return token;
        }
    }

};

module.exports = {
    Config: Config
};


