var express = require('express');
var centroCostoMdl = require('../Models/centroCostoMdl');
var softlandMdl = require('../Models/softlandMdl');
var User = require('../Models/usersMdl');
var router = express.Router();
var sendmail = require('sendmail')();
var request = require("request");
var jwt = require('jsonwebtoken');
var sgMail = require('@sendgrid/mail');
var Config = require('../config/config').Config;
var Envios = require('../Models/Enviosmail').Envios;


router.post('/', function (req, res, next) 
{
    centroCostoMdl.add(req.body.data, function (error, data) {
        if (error) {
            res.json(500, error);
        } else 
        {

            Envios.mail_validar_centro_costo(10, 'Juan ignacio Molina', '6952c78f-84e7-44c9-8bc3-6f873d94cad9' , function (error, response) 
            {
                if (error) {
                    return res.status(500).json({ msg: 'error', data: error });
                }

                return res.status(200).json({ msg: 'success', data : data });

            });
            
        }
    });
    
});

router.get('/', function (req, res, next) 
{
    var token = Config.Validate_token(req);
    user = jwt.decode(token);
    sql  = '';

    console.log(user[0].id_usuario)
    
    permisos = User.permisos(user[0].id_usuario , function (error,data)
    {

        if (error) 
        {
            res.json(500, error);

        } else 
        {

            if(data[0].ver_todos_proyectos == 1)
            {
                sql = '';
            }

            if (data[0].ver_propios_proyectos == 1) 
            {
                sql = 'AND cc.cc_jefe_proyecto = ' +  user[0].id_usuario;
            }

            if (data[0].ver_areas_proyectos == 1) 
            {
                sql = 'AND cc.cc_unidad_negocio = ' + user[0].id_area;
            }

            centroCostoMdl.getAllUser( sql , function (error, data) 
            {
                if (error) {
                    res.json(500, error);
                } else {
                    res.send(data);
                }
            });
        }
        
    })
});


router.get('/activar/:id', function(req, res,next)
{
        centroCostoMdl.getNumCC(function (error, data) 
        {
            if (error) {
                res.json(500, error);
            } else {

                centroCostoMdl.putActivacin( data[0].centro_costo , req.params.id, function (error, dto) 
                {

                    return res.status(200).json({ msg: 'success', data: data[0].centro_costo });

                });

            }
        });

});


router.post('/add/harvest/', function(req, res , next)
{
            centroCostoMdl.getId(req.body.id_proyecto , function (error, data) 
            {
                var nombre = '[ ' + data[0].centro_costo + '] ' + data[0].cc_nombre_proyecto;

                var options = {
                    method: 'POST',
                    url: 'https://api.harvestapp.com/v2/projects',
                    headers:
                        {
                            'postman-token': '48af0f47-e1da-24f4-92d8-424f249e3e96',
                            'cache-control': 'no-cache',
                            'user-agent': 'Harvest API Example',
                            authorization: 'Bearer 991990.pt.r2NZSiTK7ECK5aally7EHH5dYWDry9BWESph3bBZtGY4JrSYg-fOfcryJrcK7pXqO3URYXr8xUg-kSePGUEcJg',
                            'harvest-account-id': '495156',
                            'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
                        },
                    formData:
                        {
                            client_id: data[0].cc_empresa,
                            name: nombre,
                            is_billable: 'true',
                            bill_by: 'Project',
                            budget_by: 'project'
                        }
                };

                request(options, function (error, response, body)
                {
                    if (error) throw new Error(error);
                    return res.status(200).json({ msg: 'success', data: body });
                });

            });

});


router.get('/add/softland/:id', function (req, res, next) 
{
    centroCostoMdl.getId(req.params.id, function (error, data) 
    {
        var a = data[0].cc_unidad_negocio;
        var b = data[0].cc_tipo_negocio;

        cc = a + b + '-' + data[0].centro_costo;
        nombre = data[0].cc_nombre_proyecto;


        softlandMdl.addCentrocosto(cc , nombre  , function (error, data) 
        {
            if (error) 
            {
                res.json(500, error);

            } else 
            {
                res.status(200).json(data);
            }

        });


    });

});

router.post('/send/emailconfirmacion/', function (req, res, next) 
{
    Envios.mail_validar_centro_costo(req.body.centro_costo , req.body.cc_nombre_proyecto , 'd9da65a1-cced-466f-b9a6-d8da00772442', function (error, response)
    {
        if (error) {
            return res.status(500).json({ msg: 'error', data: error });
            next();
        }
        return res.status(200).json({ msg: 'success', data: req.body });
    });
});


router.get('/validar/', function(req, res)
{
    centroCostoMdl.getValidar(function (error, data)
    {
        if (error) 
        {
            res.json(500, error);

        } else {
            res.send(data);
        }
    });
});

router.post('/empresas', function (req, res, next) 
{
    
    centroCostoMdl.getEmpresas(req.body.data , function (error, data) 
    {
        if (error) {
            res.json(500, error);
        } else {
            res.send(data);
        }
    });

});

router.post('/find/filtrado', function (req, res, next) 
{
    sql_personas = '';
    sql_items = '';
    sql_area = '';

    if (req.body[0].area != 0)
    {
        sql_area = ' and cc_unidad_negocio = ' + req.body[0].area;
    }

    if(req.body[0].personas.length > 0)
    {
        sql_personas = ' and cc_jefe_proyecto in (' + req.body[0].personas + ')';
    }

    if (req.body[0].item.select != 0 || req.body[0].item.value != '')
    {
        sql_items = 'and ' + req.body[0].item.select + ' LIKE ' + "'%" + req.body[0].item.value + "%'";
        console.log(sql_items);
    }

    centroCostoMdl.get(sql_area , sql_personas , sql_items  , function (error, data) {
        if (error) {
            res.json(500, error);
        } else {
            res.send(data);
        }
    });

});

router.get('/softland/nv/:id', function (req, res, next) 
{
    var array= [];
    softlandMdl.getNotasdeVentas( req.params.id , function (error, data) 
    {
        if (error) 
        {
            res.json(500, error);

        } else 
        {
            softlandMdl.getTotalNotasdeVentas( req.params.id , function (error, data1) {
                if (error) 
                {

                    res.json(500, error);

                } else {

                    array = { 'datos' : data , 'total' : data1 }

                    res.status(200).json(array);

                }
            });
        }
    });  

});


router.get('/softland/ociconstruye/:id', function (req, res, next) {

    var array = [];

    var id = req.params.id;

    if (req.params.id.length == 4) {
        id = '0' + req.params.id
    }

    softlandMdl.getOcIcontruye(id , function (error, data) {
        if (error) {
            res.json(500, error);

        } else {

            softlandMdl.getTotalOcIconstruye(id , function (error, data1) {
                if (error) {

                    res.json(500, error);

                } else {

                    array = { 'datos': data, 'total': data1 }

                    res.status(200).json(array);

                }
            });

        }
    });

});


router.get('/softland/subcontratos/:id', function (req, res, next) {

    var array = [];
    
    var id = req.params.id;

    if (req.params.id.length == 4)
    {
       id =  '0' + req.params.id
    }
    
    softlandMdl.getSubcontratos(id,function (error, data) 
    {
        if (error) {
            res.json(500, error);

        } else {

            softlandMdl.getTotalSubcontratos(id,function (error, data1) {
                if (error) {
                    res.json(500, error);

                } else {

                    array = { 'datos': data, 'total': data1 }

                    res.status(200).json(array);

                }
            });

        }
    });

});

router.get('/softland/oc/:id', function (req, res, next) {

    var array = [];


    softlandMdl.getOcSoftland(req.params.id,function (error, data) 
    {
        if (error) {
            res.json(500, error);

        }
        else
        {
            softlandMdl.getTotalOcSoftland( req.params.id ,function (error, data1) {
                if (error) {
                    res.json(500, error);

                } else {

                    array = { 'datos': data, 'total': data1 }

                    res.status(200).json(array);

                }
            });

        }
    });

});

router.post('/find', function (req, res, next) 
{
    centroCostoMdl.findcC(req.body.data, function (error, data) 
    {
        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json({ items: data });
        }

    });

});

router.get('/find/:id', function (req, res, next) {

    console.log(Date.now());
    centroCostoMdl.getId( req.params.id , function (error, data) 
    {
        if (error) {
            res.json(500, error);
        } else {
            res.send(data);
        }
    });
});


router.get('/inactivos/:id', function (req, res, next) 
{
    centroCostoMdl.getInactivos(req.params.id , function (error, data) {
        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json(data);
        }
    });

});


router.put('/:id', function (req, res, next) {

    for (var key in req.body) 
    {
        var data = req.body[key];

        if (data == "null" || data == " " || data == null) {
            data = "";
        }

        if (key == "jefe_proyecto" || key == "jefe_area" || key == "id_proyecto" || key == "nombre_empresa") 
        {
            
            console.log(key);

        }else
        {
             centroCostoMdl.put(key, data, req.params.id, function (error, data) 
            {

            });
        }

    }

    res.status(200).json("Usuario actualizado");
});


router.post('/send/:id', function (req, res, next) 
{
    var para = req.body['para'];
    var _cc = req.body['cc'];
    var _cco = req.body['cco'];
    var sub = 'Creacion Centro Costo ' + req.params.id;


    sendmail({
        from: 'infocl@instore.cl' ,
        to: para,
        cc: _cc,
        bcc: _cco,
        subject: sub,
        html: 'Se creo con exito el centro costo',
    }, function (err, reply) 
    {
        console.log(err && err.stack);
        console.dir(reply);

        if(err)
        {
            res.status(500).send(err);
        
        }

        res.status(200).send('ok');

    });

   
});


//HISTORIAL

router.get('/historial/:id', function (req, res, next) {

    centroCostoMdl.getHistorial(req.params.id, function (error, data) {
        if (error) {
            res.json(500, error);
        } else {
            res.send(data);
        }
    });
});

router.post('/historial/:id', function (req, res, next) {

    centroCostoMdl.addHistorial(req.body, function (error, data) {
        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json(data);
        }
    });
    

});

// BUSQUEDA JEFES DE PROYECTOS O USUARIOS


router.get('/users/findcc/:id', function (req, res, next) {

    User.findMail(req.params.id, function (error, user) {

        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json({ items: user });
        }

    })

});



router.post('/users/find/', function (req, res, next) {

    User.find(req.body.data, function (error, user) {

        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json({ items: user });
        }

    })

});








module.exports = router;
