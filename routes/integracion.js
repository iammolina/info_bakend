var express = require('express');
var router = express.Router();
var User = require('../Models/usersMdl');
var centroCostoMdl = require('../Models/centroCostoMdl');
var { Client } = require('pg');



// CLIENTES HARVEST

router.get('/clientes/harvest' , function(req,res,next) 
{
    var data = [];
    var options = {
        method: 'GET',
        url: 'https://api.harvestapp.com/v2/clients',
        headers:
            {
                'postman-token': '48af0f47-e1da-24f4-92d8-424f249e3e96',
                'cache-control': 'no-cache',
                'user-agent': 'Harvest API Example',
                authorization: 'Bearer 991990.pt.r2NZSiTK7ECK5aally7EHH5dYWDry9BWESph3bBZtGY4JrSYg-fOfcryJrcK7pXqO3URYXr8xUg-kSePGUEcJg',
                'harvest-account-id': '495156',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
            }
    };
    request(options, function (error, response, body) 
    {
        if (error) throw new Error(error);

         data = JSON.parse(body);

        for (let index = 0; index < data['total_entries'] ; index++) 
        {
            var cliente = data['clients'];

            datos = [{ id_harvest: cliente[index].id, name: cliente[index].name, create_at:  (Date.now() / 1000), updated_at :  (Date.now() / 1000 ) }];


            centroCostoMdl.addEmpresas(datos , function (error,id)
            {

                console.log(id);

            });



        }

    });
    res.status(200).json('hola');
});

//USUARIOS INFO ANTIGUO

router.get('/usuarios/info', (req, res,next) => 
{

    const client = new Client({
        host: '10.100.100.4',
        port: 5432,
        user: 'deploy',
        password: '60a7QVkPHMkkm9UJ',
        database:'info'
    })

    client.connect()
    client.query('select status.sp_pers_id  , persona.nombre , persona.paterno , persona.materno  , persona.username from status_proyecto status INNER JOIN persona on status.sp_pers_id = persona.id  WHERE status.sp_pers_id NOTNULL GROUP BY status.sp_pers_id , persona.nombre , persona.paterno , persona.materno , persona.username', (err, data) => 
    {
        if (err) throw err

        for (let index = 0; index < data.rowCount; index++) 
        {
            userdata = [{
                id_anterior: data.rows[index].sp_pers_id,
                nombre: data.rows[index].nombre,
                apellido: data.rows[index].paterno + ' ' + data.rows[index].materno,
                correo: data.rows[index].username + '@instore.cl',
                clave: 'Welcome123',
            }]

            User.add(userdata, function (error,data)
                {
                    if (error) 
                    {
                        res.json(500, error);
                    } else 
                    {
                        console.log(data);
                    }
                })
        }

        res.send('hola');
        
    
        client.end()
    });


});



router.get('/proyectos/info', (req, res, next) => 
{
    const client = new Client({
        host: '10.100.100.4',
        port: 5432,
        user: 'deploy',
        password: '60a7QVkPHMkkm9UJ',
        database: 'info'
    })

    client.connect()
    client.query('select * from status_proyecto WHERE sp_activo = 1 AND sp_estado = 1 ORDER BY sp_area_pers , sp_pers_id', (err, data) => 
    {

        if (err) throw err

        for (let index = 0; index < data.rowCount; index++) 
        {
            User.findIdOld(data.rows[index].sp_pers_id, function (error, dta) 
            {
                if (error) {
                    res.json(500, error);
                } else {   

                    var usuario = 0;
                    const sp_area_pers = data.rows[index].sp_area_pers

                    if (dta.length > 0)
                    {
                        usuario = dta[0].id_usuario;
                    }

                    area = 0;

                    if (sp_area_pers == 1 || sp_area_pers == 9 )
                    {
                        area = 4
                    }

                    if (sp_area_pers == 2 || sp_area_pers == 18 || sp_area_pers == 6 || sp_area_pers == 4) {
                        
                        area = 2
                    }

                    if (sp_area_pers == 5 || sp_area_pers == 16) {
                        area = 1
                    }

                    if (sp_area_pers == 13 || sp_area_pers == 15 || sp_area_pers == 8)
                    {
                        area = 6
                    }

                    if ( sp_area_pers == 21 || sp_area_pers == 24)
                    {
                        area = 6
                    }

                    if (sp_area_pers == 12) {
                        area = 8
                    }

                    if (sp_area_pers == 20)
                    {
                        area = 7
                    }

                    if (sp_area_pers == 10)
                    {
                        area = 5
                    }


                    var resp = 0;
                    const responsable = data.rows[index].sp_id_resp_pres;

                    if (responsable == 27 || responsable == 3412 || responsable == 3425 || responsable == 3463 || responsable == 3515)
                    {

                        resp = 136;
                    }

                    if (responsable == 3476)
                    {
                        resp = 137
                    }

                    if (responsable == 3479) {
                        resp = 139
                    }

                    if (responsable == 3486) {
                        resp = 140
                    }

                    if (responsable == 3493) {
                        resp = 138
                    }


                    if (data.rows[index].sp_color  == 'ffffff') { est = 1 }
                    if (data.rows[index].sp_color  == 'ffc000') { est = 2 }
                    if (data.rows[index].sp_color  == '92d050') { est = 3 }
                    if (data.rows[index].sp_color  == 'fcff00') { est = 4 }
                    if (data.rows[index].sp_color  == 'ff0000') { est = 5 }



                    const fechaCotizacion = data.rows[index].sp_fecha_planimetria;
                    var fecha_cotizacion = 0;
                    const fechaPresupuesto = data.rows[index].sp_fecha_entr_oc_cli;
                    var fecha_presupuesto = 0;


                    
                    if(fechaCotizacion)
                    {
                        if (fechaCotizacion.length > 3 && fechaCotizacion != 'PENDIENTE') 
                        {
                            
                             var fecha = fechaCotizacion.split('/');
                             var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

                             f1 = new Date(f).getTime();

                             if(f1)
                             {
                                 fecha_cotizacion = new Date(f).getTime();
                             }
                        }

                    }

                    if (fechaPresupuesto) {
                        if (fechaPresupuesto.length > 3 && fechaPresupuesto != 'PENDIENTE') {

                            var fecha = fechaPresupuesto.split('/');
                            var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

                            f1 = new Date(f).getTime();

                            if (f1) {
                                fecha_presupuesto = new Date(f).getTime();
                            }
                        }

                    }
                    



                    


    
                    Value = [
                        {
                            centro_costo: data.rows[index].sp_np,
                            cc_nombre_proyecto: data.rows[index].sp_descripcion ,
                            cc_ubicacion: data.rows[index].sp_tienda ,
                            cc_cliente: data.rows[index].sp_cliente ,
                            cc_direccion: ' ',
                            cc_superficie: data.rows[index].sp_mtr ,
                            cc_unidad_negocio: area ,
                            cc_tipo_negocio: 1 , 
                            cc_jefe_area: 1 ,
                            cc_jefe_proyecto: usuario,
                            estado: data.rows[index].sp_estado ,
                            fase: est ,
                            fecha_activacion: 0 ,
                            cc_fecha_planimetria: (fecha_cotizacion / 1000),
                            cc_fecha_presupuesto: (fecha_presupuesto / 1000),
                            cc_fecha_inicio:  0,
                            cc_fecha_termino: 0,
                            responsable: resp,
                            create_at: 0
                        }
                    ]

                    centroCostoMdl.add(Value, function (error, id) 
                    {
                        if (error) {
                            res.json(500, error);
                        } else 
                        {
                            const obj =
                            {
                                id_usuario: 135,
                                id_centro_costo: id,
                                observacion: data.rows[index].sp_obs_gestion,
                                create_at: ( new Date().getTime() / 1000 )
                            }

                            if (data.rows[index].sp_obs_gestion)
                            {

                                if (data.rows[index].sp_obs_gestion.length > 3) 
                                {
                                    centroCostoMdl.addHistorial(obj, function (error, data) {
                                        if (error) {
                                            res.json(500, error);
                                        } else {

                                            console.log(data)
                                        }
                                    });
                                }

                            }
                           
                        }
                    })
                    

                       
                    }

                    
              

            })


           
        }

        console.log('terminado')

        res.send('hola');

        client.end()
    });


});







module.exports = router;
