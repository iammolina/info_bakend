var express = require('express');
var router = express.Router();
var User = require('../Models/usersMdl');
var jwt = require('jsonwebtoken');
var Config = require('../config/config').Config;
var softlandMdl = require('../Models/softlandMdl');
var centroCostoMdl = require('../Models/centroCostoMdl');
var fileUpload = require('express-fileupload');

router.use(fileUpload());



router.get("/:id", function (req, res, next)
{
 
    softlandMdl.getTotalNotasdeVentas(req.params.id , function (error, data) 
    {
        var monto_neto_nv = 0;
        var monto_facturado = 0;
        var pendiente = 0;
       
        if (error)
        {
            res.json(500, error);

        } else 
        {
            if (data[0].monto_neto)
            {
                monto_neto_nv = data[0].monto_neto;
            }
            

            centroCostoMdl.getFaturar(req.params.id, function (error, data1) 
            {
                if (error) {
                    res.json(500, error);
                } else 
                {
                    if (data1[0].monto)
                    {
                        monto_facturado = data1[0].monto;
                    }
                  
                    pendiente = monto_neto_nv - monto_facturado;

                    array =
                    {
                        mnnv : monto_neto_nv,
                        mf : monto_facturado,
                        pen: pendiente
                    }
                  
                    res.status(200).json(array);
                    
                }
            });


        }
    });

});


router.get("/fact/solicitadas/:id", function (req, res, next) 
{
    centroCostoMdl.getFaturarSolicitadas( req.params.id , function (error, data) {
        if (error) {
            res.json(500, error);
        } else {

            res.status(200).json(data);
        }
    });

});


router.post("/fact/solicitadas", function (req, res, next) 
{
    var obj = 
    {
        cc : req.body.cc,
        porciento : req.body.porcentaje,
        id_solicita : 1,
        estado : 1 ,
        detalle : req.body.detalle,
        monto: req.body.monto_facturar,
        oc: req.body.Numoc,
        fecha_solicita: new Date
    }

    
    centroCostoMdl.addFacturacion( obj ,function (error, id) 
    {
        if (error) {
            res.json(500, error);
        } else 
        {

            if (req.files.file)
            {
                let imageFile = req.files.file;

                imageFile.mv(`./public/oc/${id}.jpg`, function (err)
                {
                    if (err) {
                        return res.status(500).send(err);
                    }

                });
            }

            res.json({ file: id });

        }

    });

});

router.put("/fact/solicitadas", function (req, res, next) 
{
    centroCostoMdl.putFactura(req.body.fact.id, req.body.fact.monto, req.body.fact.oc, req.body.fact.detalle, function (error, data) 
    {
        if (error) {
            res.json(500, error);
        } else {

            res.status(200).json(data);
        }
    });
});

router.put("/fact/solicitadas/estado", function (req, res, next) 
{
    centroCostoMdl.putFacturaEstado(req.body.id , req.body.estado , function (error, data) 
    {
        if (error) {
            res.json(500, error);
        } else {

            res.status(200).json(data);
        }
    });
    
});


router.put("/fact/solicitadas/aceptar", function (req, res, next) {
    centroCostoMdl.putFacturaAceptado(req.body.id,  req.body.num_factura  ,req.body.estado, req.body.id_realizado , req.body.fecha_realizado ,  function (error, data) {
        if (error) {
            res.json(500, error);
        } else {
            res.status(200).json(data);
        }
    });

});





module.exports = router;
